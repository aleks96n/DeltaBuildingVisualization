﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ChangeIcon : MonoBehaviour {

    // selles klassis vahetatakse ilamstiku logo

    private IEnumerator coroutine;

    public void changeIcon(WeatherJson weather, Image img, GameObject image)
    {
        // aktiveeritakse logo pilti (Unity.Image)
        image.SetActive(true);
        WeatherImage weat = new WeatherImage();
        try
        {
            if (weather.weather[0].id == 800)
            {
                // selge päikesepaisteline ilm
                coroutine = weat.GetTexture("01", img);
                StartCoroutine(coroutine);
            }
            if (weather.weather[0].id == 801)
            {
                // vahelduv pilvisus
                coroutine = weat.GetTexture("02", img);
                StartCoroutine(coroutine);
            }
            if (weather.weather[0].id == 802)
            {
                // selgimistega pilvisus
                coroutine = weat.GetTexture("03", img);
                StartCoroutine(coroutine);
            }
            if (weather.weather[0].id >= 803 && weather.weather[0].id <= 804)
            {
                // pilvisus
                coroutine = weat.GetTexture("04", img);
                StartCoroutine(coroutine);
            }
            if (weather.weather[0].id >= 500 && weather.weather[0].id <= 504)
            {
                // vihmasaju
                coroutine = weat.GetTexture("10", img);
                StartCoroutine(coroutine);
            }
            if (weather.weather[0].id >= 511 && weather.weather[0].id <= 531)
            {
                // hoovihm
                coroutine = weat.GetTexture("09", img);
                StartCoroutine(coroutine);
            }
            if ((weather.weather[0].id >= 802 && weather.weather[0].id <= 804) || (weather.weather[0].id >= 300 && weather.weather[0].id <= 321))
            {
                // selgimistega pilvisus
                coroutine = weat.GetTexture("03", img);
                StartCoroutine(coroutine);
            }
            if (weather.weather[0].id >= 200 && weather.weather[0].id <= 232)
            {
                // äike
                coroutine = weat.GetTexture("11", img);
                StartCoroutine(coroutine);
            }
            if (weather.weather[0].id >= 600 && weather.weather[0].id <= 622)
            {
                // lumesadu
                coroutine = weat.GetTexture("13", img);
                StartCoroutine(coroutine);
            }
            if (weather.weather[0].id >= 701 && weather.weather[0].id <= 781)
            {
                // udu
                coroutine = weat.GetTexture("50", img);
                StartCoroutine(coroutine);
            }



        }
        catch
        {
            // pildi viga
        }


    }
}
