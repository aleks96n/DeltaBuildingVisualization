﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherController : MonoBehaviour
{
    // ilma kontrollimise skript, loob uue WeatherLoad objekti

    WeatherLoad weatherJSON;

    void Start()
    {
        weatherJSON = new WeatherLoad();
        Debug.Log(weatherJSON.weather);
        StartCoroutine("GiveItSomeTime");

    }

    IEnumerator GiveItSomeTime()
    {

        while (weatherJSON.weather == null)
        {
            yield return new WaitForSeconds(0.3f);
        }

    }
}
