﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunColor : MonoBehaviour {

    // skript visualiseerib päikese värvuse muutmist. Nii õhtul ja hommiku päike on punase tooniga ja keskpäeval kollane

    public Light sun;
    string formatString = "HH'*'mm'*'ss";

    string hours;
    string minutes;
    string seconds;

    DateTime CompareTimeNow;
    DateTime CompareTime_9_00;
    DateTime CompareTime_18_00;

    private DateTime myTime;
    private TimeSpan time;

    public Color red;
    public Color yellow;

    float smoothness = 0.1f;
    float duration = 1800f;

    Color SunClr;
    
    private IEnumerator Coroutine;

    void Start () {
        SunClr = sun.color;
        string sampleData_9_00 = "09*00*00";
        string sampleData_18_00 = "18*00*00";
        CompareTime_9_00 = DateTime.ParseExact(sampleData_9_00, formatString, null);
        CompareTime_18_00 = DateTime.ParseExact(sampleData_18_00, formatString, null);


    }
	
	void Update () {

        // uuendakse kellaaega

        myTime = DateTime.Now;
        time = DateTime.Now.TimeOfDay;

        if (time.Hours.ToString().Length == 1) { hours = "0" + time.Hours; } else { hours = time.Hours.ToString(); }
        if (time.Minutes.ToString().Length == 1) { minutes = "0" + time.Minutes; } else { minutes = time.Minutes.ToString(); }
        if (time.Seconds.ToString().Length == 1) { seconds = "0" + time.Seconds; } else { seconds = time.Seconds.ToString(); }
        string sample = hours + "*" + minutes + "*" + seconds;

        CompareTimeNow = DateTime.ParseExact(sample, formatString, null);

        // kui kell on 9:00, siis muudetakse suundvalguallika värvuse kollaseks
        if(CompareTime_9_00 == CompareTimeNow)
        {
            Coroutine = SunToYellow(smoothness, duration);
            StartCoroutine(Coroutine);
        }

        // kui kell on 18:00, siis muudetakse suundvalguallika värvuse kollaseks punase tooniga
        if (CompareTime_18_00 == CompareTimeNow)
        {
            Coroutine = SunToRed(smoothness, duration);
            StartCoroutine(Coroutine);
        }
    }

    public IEnumerator SunToRed(float smoothness, float duration)
    {
        // sujuvalt värvi muutmine kollaseks punase tooniga
        float progress = 0;
        float increment = smoothness / duration;
        while (progress < 1)
        {
            sun.color = Color.Lerp(SunClr, red, progress);
            progress += increment;
            yield return new WaitForSeconds(smoothness);
        }
        SunClr = sun.color;
        yield return true;
    }

    public IEnumerator SunToYellow(float smoothness, float duration)
    {
        // sujuvalt värvi muutmine kollaseks
        float progress = 0;
        float increment = smoothness / duration;
        while (progress < 1)
        {
            sun.color = Color.Lerp(SunClr, yellow, progress);
            progress += increment;
            yield return new WaitForSeconds(smoothness);
        }
        SunClr = sun.color;
        yield return true;
    }
}
