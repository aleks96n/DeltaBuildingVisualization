﻿using System.Collections.Generic;

// abifail JSON avamiseks

[System.Serializable]
public class Subject
{
    public string Start;
    public string Name;
    public string Name_Eng;
    public string Type;
    public int Students;
    public string Teacher;
}

[System.Serializable]
public class Room
{
    public string Id;
    public List<Subject> Subject;
}

[System.Serializable]
public class Monday
{
    public List<Room> Rooms;
}

[System.Serializable]
public class Tuesday
{
    public List<Room> Rooms;
}



[System.Serializable]
public class Wednesday
{
    public List<Room> Rooms;
}



[System.Serializable]
public class Thursday
{
    public List<Room> Rooms;
}



[System.Serializable]
public class Friday
{
    public List<Room> Rooms;
}

[System.Serializable]
public class TimetableJson
{
    public Monday Monday;
    public Tuesday Tuesday;
    public Wednesday Wednesday;
    public Thursday Thursday;
    public Friday Friday;
}