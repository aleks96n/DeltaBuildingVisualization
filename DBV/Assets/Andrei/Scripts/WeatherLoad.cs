﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class WeatherLoad : MonoBehaviour
{
    // skript vastutab õige ilma visualisatsiooni käima panemisel

    private string url = "http://api.openweathermap.org/data/2.5/weather?q=Tartu&mode=json&units=metric&APPID=7cc077cc0e9b344bd8477f966ced0a4b";
    private string jsonstring;
    public WeatherJson weather;
    public WeatherJson weatherComp;
    bool first = true;
    bool isNetwork = false;

    public Light sun;

    public Material snow;
    public Material grass;
    public Material current;

    private float t = 0;

    float timeForParticle = 300;

    public ParticleSystem rainPart;
    public ParticleSystem snowPart;
    public bool Rain = false;
    public bool Snow = false;
    public bool lightningBool = false;

    public bool SnowHills = false;
    public bool RainPuddles = false;
    
    public bool puddlesBool = false;
    
    public bool snowGrass = false;


    float duration = 30f;
    float CloudsDuration = 30;

    float smoothness = 0.1f;

    private IEnumerator coroutine;
    private IEnumerator coroutineLightning;

    public List<GameObject> puddles;
    Animator puddleAnim;

    public List<GameObject> snowHillsObj;
    Animator snowHillsAnim;

    public Image img;
    public Text degrees;

    public GameObject image;
    private UnityWebRequest www;



    IEnumerator Start()
    {
        StartCoroutine("testNetwork");
        while (true)
        {
            // päring OpenWeatherMap rakendusliidestusse
            using (www = UnityWebRequest.Get(url))
            {
                yield return www.SendWebRequest();
                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(www.error);
                }
                else
                {
                    jsonstring = www.downloadHandler.text;
                }
            }
            
            // päringu saadud anmdete avamine JSON formaadis ning andmete uuendamine iga 10 sekundid
            string abc = JsonUtility.ToJson(jsonstring);
            weather = JsonUtility.FromJson<WeatherJson>(jsonstring);
            if (first)
            {
                //weatherComp = weather;
                first = false;
            }
            yield return new WaitForSeconds(10);
        }
    }

    void Update()
    {
        if (isNetwork)
        {
            // kui ilma andmed muutusid, siis tehakse visualisatsioonil muutusi ka
            if (!comptemp(weather, weatherComp) || !compclouds(weather, weatherComp) || !compID(weather, weatherComp))
            {
                // õhitemperatuuri ja pildi visualiseerimine

                degrees.text = weather.main.temp + " °C";
                ChangeIcon ch = gameObject.AddComponent<ChangeIcon>();
                ch.changeIcon(weather, img, image);


                if (!compID(weather, weatherComp))
                {
                    try
                    {
                        if (weather.weather[0].id >= 200 && weather.weather[0].id <= 232)
                        {
                            // muudatused äikese puhul
                            if (!Rain)
                            {
                                coroutine = StartParticle(2400, 300, rainPart);
                                StartCoroutine(coroutine);

                            }

                            if (Snow)
                            {
                                coroutine = StopAndStart(snowPart, rainPart, 2400);
                                StartCoroutine(coroutine);
                                if (SnowHills)
                                {
                                    LerpController lc = new LerpController();
                                    coroutine = lc.LerpColor_toGrass(current, grass);
                                    StartCoroutine(coroutine);
                                    snowGrass = false;

                                    ObjectController ob = new ObjectController();
                                    coroutine = ob.deleteSnowHills(snowHillsObj, snowHillsAnim, 1000);
                                    StartCoroutine(coroutine);
                                    SnowHills = false;
                                }
                            }
                            if (!puddlesBool)
                            {
                                ObjectController ob = new ObjectController();
                                coroutine = ob.makePuddles(puddles, puddleAnim, 500);
                                StartCoroutine(coroutine);
                                puddlesBool = true;
                            }

                            lightningBool = true;
                            StartCoroutine("lightning");
                        }

                        if (weather.weather[0].id >= 300 && weather.weather[0].id <= 321)
                        {
                            // muudatused nõrga vihama puhul
                            lightningBool = false;
                            if (!Rain)
                            {
                                coroutine = StartParticle(1000, 300, rainPart);
                                StartCoroutine(coroutine);

                            }

                            if (Snow)
                            {
                                coroutine = StopAndStart(snowPart, rainPart, 1000);
                                StartCoroutine(coroutine);
                            }

                            if (Rain)
                            {
                                coroutine = DecreaseParticle(90, rainPart, 1000);
                                StartCoroutine(coroutine);
                            }

                            if (!puddlesBool)
                            {
                                ObjectController ob = new ObjectController();
                                coroutine = ob.makePuddles(puddles, puddleAnim, 1000);
                                StartCoroutine(coroutine);
                                puddlesBool = true;
                            }
                        }

                        if (weather.weather[0].id >= 500 && weather.weather[0].id <= 531)
                        {
                            // muudatused vihama puhul
                            lightningBool = false;
                            if (!Rain && !Snow && !lightningBool)
                            {
                                coroutine = StartParticle(2400, 300, rainPart);
                                StartCoroutine(coroutine);

                            }

                            if (Snow)
                            {
                                coroutine = StopAndStart(snowPart, rainPart, 2400);
                                StartCoroutine(coroutine);
                                Snow = false;
                                Rain = true;
                            }
                            if (Rain)
                            {
                                coroutine = IncreaseParticle(90, rainPart, 2400);
                                StartCoroutine(coroutine);
                            }
                            if (SnowHills)
                            {
                                // lumi sulab
                                LerpController lc = new LerpController();
                                coroutine = lc.LerpColor_toGrass(current, grass);
                                StartCoroutine(coroutine);
                                snowGrass = false;

                                ObjectController ob = new ObjectController();
                                coroutine = ob.deleteSnowHills(snowHillsObj, snowHillsAnim, 1000);
                                StartCoroutine(coroutine);
                                SnowHills = false;

                            }
                            if (!puddlesBool)
                            {
                                ObjectController ob = new ObjectController();
                                coroutine = ob.makePuddles(puddles, puddleAnim, 1000);
                                StartCoroutine(coroutine);
                                puddlesBool = true;
                            }
                        }

                        if (weather.weather[0].id >= 600 && weather.weather[0].id <= 622)
                        {
                            // muudatused lume puhul

                            lightningBool = false;

                            if (!Snow && !Rain && !lightningBool)
                            {
                                coroutine = StartParticle(6000, 300, snowPart);
                                StartCoroutine(coroutine);

                            }

                            if (Rain)
                            {
                                coroutine = StopAndStart(rainPart, snowPart, 6000);
                                StartCoroutine(coroutine);
                            }

                            if (!SnowHills && !snowGrass)
                            {
                                if (weather.main.temp > 0)
                                {
                                    LerpController lc = new LerpController();
                                    coroutine = lc.LerpColor_toSnow(current, snow);
                                    StartCoroutine(coroutine);
                                    snowGrass = true;

                                    ObjectController ob = new ObjectController();
                                    coroutine = ob.makeSnowHills(snowHillsObj, snowHillsAnim, 5000);
                                    StartCoroutine(coroutine);
                                    SnowHills = true;
                                }
                            }




                        }

                        if (weather.weather[0].id >= 800 && weather.weather[0].id <= 804)
                        {
                            // muudatused päikesepaiste puhul

                            lightningBool = false;
                            if (Rain || lightningBool)
                            {
                                coroutine = StopParticle(300, rainPart);
                                StartCoroutine(coroutine);
                                if (puddlesBool)
                                {
                                    ObjectController ob = new ObjectController();
                                    coroutine = ob.deletePuddles(puddles, puddleAnim, 500);
                                    StartCoroutine(coroutine);
                                    SnowHills = false;
                                }
                            }

                            if (Snow)
                            {
                                coroutine = StopParticle(300, snowPart);
                                StartCoroutine(coroutine);


                                if (weather.main.temp > 0 && SnowHills || weather.main.temp > 0 && snowGrass)
                                {
                                    if (SnowHills)
                                    {
                                        ObjectController ob = new ObjectController();
                                        coroutine = ob.deleteSnowHills(snowHillsObj, snowHillsAnim, 500);
                                        StartCoroutine(coroutine);
                                        SnowHills = false;

                                        LerpController lc2 = new LerpController();
                                        coroutine = lc2.LerpColor_toGrass(current, grass);
                                        StartCoroutine(coroutine);
                                        snowGrass = false;

                                    }
                                    else
                                    {
                                        LerpController lc3 = new LerpController();
                                        coroutine = lc3.LerpColor_toGrass(current, grass);
                                        StartCoroutine(coroutine);
                                        snowGrass = false;
                                    }
                                }

                            }
                        }
                        weatherComp = weather;
                    }
                    catch
                    {
                        // viga
                    }
                }

                if (!compclouds(weather, weatherComp))
                {
                    float end = 1 - (Convert.ToSingle(weather.clouds.all) / 200);
                    coroutine = ChangeClouds(sun.intensity, end);
                    StartCoroutine(coroutine);

                }
            }
        }
    }

    IEnumerator lightning()
    {
        // äike visualiseerimine [1. 100] sekundi ajavahemikus
        while (lightningBool)
        {
            System.Random rnd = new System.Random();
            int time = rnd.Next(1, 101);
            SunCollor col = new SunCollor();
            coroutineLightning = col.lightning(sun);
            StartCoroutine(coroutineLightning);

            yield return new WaitForSeconds(time);
        }

    }

    bool comptemp(WeatherJson weather, WeatherJson weatherComp)
    {
        // õhutemperatuuri andmete võrdlemine
        try
        {
            return weather.main.temp == weatherComp.main.temp;
        }
        catch { return false; }
    }
    bool compclouds(WeatherJson weather, WeatherJson weatherComp)
    {
        // pilvisuse võrdlemine
        try
        {
            return weather.clouds.all == weatherComp.clouds.all;
        }
        catch { return false; }
    }
    bool compID(WeatherJson weather, WeatherJson weatherComp)
    {
        // ilam ID võrdlemine
        try
        {
            return weather.weather[0].id == weatherComp.weather[0].id;
        }
        catch { return false; }
    }




    IEnumerator ChangeClouds(float start, float end)
    {
        // pilvisuse sujuv visualiseerimine (suundvalgusallika heleduse muutmine)
        float progress = 0;
        float increment = smoothness / CloudsDuration;
        while (progress < 1)
        {
            sun.intensity = Mathf.Lerp(start, end, progress);
            progress += increment;
            yield return new WaitForSeconds(smoothness);
        }
        yield return true;
    }

    IEnumerator StartParticle(int power, float timeForParticle, ParticleSystem part)
    {
        // osakestesüsteemi algus
        part.gameObject.SetActive(true);
        while (part.maxParticles < power)
        {
            part.maxParticles += Convert.ToInt32(power / timeForParticle);
            Debug.Log(Convert.ToInt32(power / timeForParticle));
            yield return new WaitForSeconds(1);
        }

        if (part.name == "rain")
        {
            Rain = true;
        }
        if (part.name == "snow")
        {
            Snow = true;
        }
    }

    IEnumerator StopParticle(float timeForParticle, ParticleSystem part)
    {
        // osakestesüsteemi lõpp
        float ab = part.maxParticles / timeForParticle;
        Debug.Log(Convert.ToInt32(ab));
        while (part.maxParticles > 0)
        {
            part.maxParticles -= Convert.ToInt32(ab);
            yield return new WaitForSeconds(1);
        }

        part.gameObject.SetActive(false);

        if (part.name == "rain")
        {
            Rain = false;
        }
        if (part.name == "snow")
        {
            Snow = false;
        }

    }

    IEnumerator DecreaseParticle(float timeForParticle, ParticleSystem part, int numberOfParticles)
    {
        // osakeste koguse (MaxParticles) vähendamine 
        float ab = part.maxParticles / timeForParticle;
        while (part.maxParticles > numberOfParticles)
        {
            part.maxParticles -= Convert.ToInt32(ab);
            yield return new WaitForSeconds(1);
        }
    }

    IEnumerator IncreaseParticle(float timeForParticle, ParticleSystem part, int numberOfParticles)
    {
        // osakeste koguse (MaxParticles) suurendamine 
        float ab = part.maxParticles / timeForParticle;
        while (part.maxParticles < numberOfParticles)
        {
            part.maxParticles += Convert.ToInt32(ab);
            yield return new WaitForSeconds(1);
        }
    }

    IEnumerator StopAndStart(ParticleSystem stop, ParticleSystem start, int numberOfParticles)
    {
        // ühe osakestesüsteemi lõpp ja teise algus
        coroutine = StopParticle(120, stop);
        StartCoroutine(coroutine);
        yield return new WaitForSeconds(130);
        coroutine = StartParticle(numberOfParticles, 300, start);
        StartCoroutine(coroutine);
        if (stop.name == "rain")
        {
            Rain = false;
            Snow = true;
        }
        else
        {
            Snow = false;
            Rain = true;
        }
        yield return true;
    }

    IEnumerator testNetwork()
    {
        while (true)
        {
            // internetiga [henduse kontrollimine
            using (UnityWebRequest req = UnityWebRequest.Get("www.google.com"))
            {
                yield return req.SendWebRequest();
                if (req.isNetworkError || req.isHttpError)
                {
                    isNetwork = false;
                }
                else
                {
                    isNetwork = true;
                }
            }
            yield return new WaitForSeconds(10f);
        }
    }
}

