﻿using System.Collections;
using UnityEngine;

public class LerpController {

    // siin muudetakse sujuvalt rohu värvust

    float smoothness = 0.1f;
    float durationIncrease = 3600;
    float durationDecrease = 10800;
    Color current2;

    public IEnumerator LerpColor_toSnow( Material current, Material snow)
    {
        current2 = current.color;
        yield return new WaitForSeconds(420); // enne värvuse muutmist oodatakse 420 sekundit (120 esimese ilmastiu lõpetamine + 300 teise ilmastiku alustamine) ilmastiku visualisatsiooni muutmiseks
        float progress = 0; 
        float increment = smoothness / durationIncrease;
        while (progress < 1)
        {
            current.color = Color.Lerp(current2, snow.color, progress); // kasutatakse Lerp funktsiooni sujuva värvuse muutmiseks
            progress += increment;
            yield return new WaitForSeconds(smoothness);
        }
        
        yield return true;
    }

    public IEnumerator LerpColor_toGrass( Material current, Material grass)
    {
        current2 = current.color;
        yield return new WaitForSeconds(420); // enne värvuse muutmist oodatakse 420 sekundit (120 esimese ilmastiu lõpetamine + 300 teise ilmastiku alustamine) ilmastiku visualisatsiooni muutmiseks
        float progress = 0;
        float increment = smoothness / durationDecrease;
        while (progress < 1)
        {
            current.color = Color.Lerp(current2, grass.color, progress); // kasutatakse Lerp funktsiooni sujuva värvuse muutmiseks
            progress += increment;
            yield return new WaitForSeconds(smoothness);
        }
        //snowGrass = false;
        yield return true;
    }
}
