﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;
using System;

public class CumulocityListener : MonoBehaviour {


    protected class InitialData {
        public string id = "1";
        public string[] supportedConnectionTypes = { "long-polling" };
        public string channel = "/meta/handshake";
        public string version = "1.0";
    }

    protected class ResponseData
    {
        public object[] responses = {
        };
    }

    protected class InitialResponseData
    {
        public string id;
        public string[] supportedConnectionTypes;
        public string channel;
        public string version;
        public string clientId;
        public string minimumVersion;
        public bool successful;
    }

    protected class SubscribeData
    {
        public string id = "2";
        public string channel = "/meta/subscribe";
        public string clientId = "";
        public string subscription = "/Liikumine/*";
    }

    protected class LongPollData
    {
        public string id = "3";
        public string connectionType = "long-polling";
        public string channel = "/meta/connect";
        public string clientId = "";
    }

    protected UnityWebRequest request;
    protected string auth;

    protected Action<string> callback;

    protected string clientId;


    void Start()
    { 
        // Cumulocity kasutaja tunnused
        auth = "andrei.voitenko96:AndreiVoitenko";
        auth = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(auth));
        auth = "Basic " + auth;

        StepZero();        
    }

    protected void StepZero()
    {
        // päringu saatmine Cumulocity sisselogimiseks
        SendRequest("https://vis3d.iot.cs.ut.ee/cep/notifications", new InitialData());
        callback = StepOne;
    }

    protected void StepOne(string data)
    {
        // päringu saatmine Cumulocity sündmuste tellimiseks
        InitialResponseData dataObject = JsonUtility.FromJson<InitialResponseData>(data);
        clientId = dataObject.clientId;
        SubscribeData subscribeDataObject = new SubscribeData();
        subscribeDataObject.clientId = clientId;
        SendRequest("https://vis3d.iot.cs.ut.ee/cep/notifications", subscribeDataObject);
        callback = StepTwo;
    }

    protected void StepTwo(string data)
    {
        // päringu saatmine Cumulocity sündmuste pikk kutsumine
        LongPollData longPollDataObject = new LongPollData();
        longPollDataObject.clientId = clientId;
        SendRequest("https://vis3d.iot.cs.ut.ee/cep/notifications", longPollDataObject);
        callback = StepThree;
    }

    protected void StepThree(string data)
    {
        // tagastus pika kutsumisele
        StepTwo("");
    }

    public void ReturnCumulocity(string data)
    {
        // eraldatakse "HumanCount" andmeid ja edastatakse A. Nikolajevi ossa
        string[] splittedData = data.Split (new string[]{"},{"},StringSplitOptions.None);
		string data2 = splittedData[0] + "}";
		PostManData jsonstring = JsonUtility.FromJson<PostManData>(data2);
		string roomName = jsonstring.data.source.id;
		int amount = int.Parse(jsonstring.data.text);
        SpawnBasedOnData SBD = GameObject.FindGameObjectWithTag("Event").GetComponent<SpawnBasedOnData>();
        SBD.SetCumulocityDate(roomName, amount);

    }

    protected void SendRequest(string url, object dataObject)
    {
        // päringu saatmine Cumulocity serverile
        string data = JsonUtility.ToJson(dataObject);
        request = UnityWebRequest.Post(url, "");

        UploadHandlerRaw uH = new UploadHandlerRaw(GetBytes(data));
        request.uploadHandler = uH;
        request.uploadHandler.contentType = "application/json";
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json");
        request.SetRequestHeader("Authorization", auth);

        request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            // kui päringu saatmisel tuleb viga, siis konsooli kirjutatakse selle informatsiooni
            Debug.Log(request.error);
        }
        else
        {
            // kui päringu saatmine õnnestus, siis oodatakse 0,3 sekundid selleks, et server vastaks
            StartCoroutine("GiveItSomeTime");
        }
    }

    protected static byte[] GetBytes(string str)
    {
        // string -> bytes
        byte[] bytes = Encoding.UTF8.GetBytes(str);
        return bytes;
    }


    IEnumerator GiveItSomeTime()
    {
        while(!request.isDone)
        {
            // 0,3 sekundi ootamine
            yield return new WaitForSeconds(0.3f);
        }

        if (request.responseCode == 401)
        {
            // autoriseerimise vea väljastamine
            Debug.Log("Not authorized");
        }
        
        string data = request.downloadHandler.text;

        try
        {
            // eraldatakse vajaliku informatsiooni edasiseks saatmiseks A. Nikolaajevi ossa
            data = data.Remove(0, 1);
            data = data.Remove(data.Length - 1, 1);
            if(!data.Contains("supportedConnectionTypes") && !data.Contains("subscription"))
            {
                ReturnCumulocity(data);
                
            }
        }
        catch
        {
            // vea puhul alustatakse algusest
            StepZero();
        }

        if (request.responseCode == 200)
        {
            // edukas serveri vastus -> uus samm
            if (null != callback) { callback(data); }
        }
    }

}



