﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeDate : MonoBehaviour {

    // kellaaja visualiseerimine tunniplaani ülemises osas

    public Text tekst;

	void Update () {
        System.DateTime myTime = System.DateTime.Now;
        string hour = myTime.Hour.ToString();
        if(hour.Length < 2)
        {
            hour = "0" + hour;
        }

        string minutes = myTime.Minute.ToString();
        if (minutes.Length < 2)
        {
            minutes = "0" + minutes;
        }

        tekst.text = hour + ":" + minutes + "  " + myTime.Date.Day + "/" + myTime.Date.Month + "/" + myTime.Date.Year;
    }
}
