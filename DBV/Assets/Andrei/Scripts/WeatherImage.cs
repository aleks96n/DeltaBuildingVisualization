﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class WeatherImage{

    // päring ilmastiku pildi saamiseks

    public Texture myTexture;
    public Image img;
    private IEnumerator coroutine;
    
    public IEnumerator GetTexture(string number, Image img)
    {
        string url = "http://openweathermap.org/img/w/" + number + "d.png";
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            // päringu viga
            Debug.Log(www.error);
        }
        else
        {
            myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
        }

        // pildi kuvamine ekraanile
        img.sprite = Sprite.Create(myTexture as Texture2D, new Rect(0.0f, 0.0f, 50, 50), new Vector2(5, 5), 100.0f);
    }
}
