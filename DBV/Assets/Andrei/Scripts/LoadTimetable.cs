﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;

public class LoadTimetable : MonoBehaviour {
    public Text ruums;
    public Text subjects;
    public Text noSubjects;

    string path;
    string jsonstring;
	string day;

    private TimetableJson timetable;
    private DateTime myTime;

    string hours;
    string minutes;
    string seconds;

    string formatString = "HH'*'mm'*'ss";

    bool ThereIsSubjects = false;

    TimeSpan time;

    DateTime CompareTime;
    DateTime CompareTime_8_15;
    DateTime CompareTime_10_15;
    DateTime CompareTime_12_15;
    DateTime CompareTime_14_15;
    DateTime CompareTime_16_15;
    DateTime CompareTime_18_15;

    bool Est;

    void Start () {

        // tunniplaani muutmise ajad
        string sampleData_8_15 = "08*15*00";
        string sampleData_10_15 = "10*15*00";
        string sampleData_12_15 = "12*15*00";
        string sampleData_14_15 = "14*15*00";
        string sampleData_16_15 = "16*15*00";
        string sampleData_18_15 = "18*15*00";

        // tunniplaani faili avamine JSON formaadis
        path = Application.streamingAssetsPath + "/timetable_new.json";
        jsonstring = File.ReadAllText(path);
        timetable = JsonUtility.FromJson<TimetableJson>(jsonstring);

        // string -> DateTime
        CompareTime_8_15 = DateTime.ParseExact(sampleData_8_15, formatString, null);
        CompareTime_10_15 = DateTime.ParseExact(sampleData_10_15, formatString, null);
        CompareTime_12_15 = DateTime.ParseExact(sampleData_12_15, formatString, null);
        CompareTime_14_15 = DateTime.ParseExact(sampleData_14_15, formatString, null);
        CompareTime_16_15 = DateTime.ParseExact(sampleData_16_15, formatString, null);
        CompareTime_18_15 = DateTime.ParseExact(sampleData_18_15, formatString, null);

        // tunniplaani uuendamine iga 10 sekundi
        InvokeRepeating("updateTimetable", 1.0f, 10f);
    }
	

	void Update () {

        // reaalaja uuendamine
        myTime = DateTime.Now;
        time = DateTime.Now.TimeOfDay;

        // nullide lisamine kellaaega (nt. 5:01:07 -> 05:01:07)
        if (time.Hours.ToString().Length == 1) { hours = "0" + time.Hours; } else { hours = time.Hours.ToString(); }
        if (time.Minutes.ToString().Length == 1) { minutes = "0" + time.Minutes; } else { minutes = time.Minutes.ToString(); }
        if (time.Seconds.ToString().Length == 1) { seconds = "0" + time.Seconds; } else { seconds = time.Seconds.ToString(); }
        string sample = hours + "*" + minutes + "*" + seconds;

        // reaalaja string -> DateTime
        CompareTime = DateTime.ParseExact(sample, formatString, null);
        time = DateTime.Now.TimeOfDay;
    }

    void updateTimetable()
    {
        // tunniplaani uuendamine vaadates nädalapäeva, kellaaja, klassiruumi
        ruums.text = "";
        subjects.text = "";

        day = myTime.DayOfWeek.ToString();

        if (day == "Monday")
        {
            foreach (Room a in timetable.Monday.Rooms)
            {
                if (CompareTime_8_15 < CompareTime && CompareTime < CompareTime_10_15)
                {
                    foreach(Subject sub in a.Subject)
                    {
                        if (sub.Start == "08:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    } 
                }
                if (CompareTime_10_15 < CompareTime && CompareTime < CompareTime_12_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "10:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
                if (CompareTime_12_15 < CompareTime && CompareTime < CompareTime_14_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "12:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
                if (CompareTime_14_15 < CompareTime && CompareTime < CompareTime_16_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "14:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
                if (CompareTime_16_15 < CompareTime && CompareTime < CompareTime_18_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "16:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
            }
        }
        if (day == "Tuesday")
        {
            Debug.Log(CompareTime);
            foreach (Room a in timetable.Tuesday.Rooms)
            {
                if (CompareTime_8_15 < CompareTime && CompareTime < CompareTime_10_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "08:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
                if (CompareTime_10_15 < CompareTime && CompareTime < CompareTime_12_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "10:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
                if (CompareTime_12_15 < CompareTime && CompareTime < CompareTime_14_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "12:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
                if (CompareTime_14_15 < CompareTime && CompareTime < CompareTime_16_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "14:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
                if (CompareTime_16_15 < CompareTime && CompareTime < CompareTime_18_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "16:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
            }
        }
        if (day == "Wednesday")
        {
            foreach (Room a in timetable.Wednesday.Rooms)
            {
                if (CompareTime_8_15 < CompareTime && CompareTime < CompareTime_10_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "08:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
                if (CompareTime_10_15 < CompareTime && CompareTime < CompareTime_12_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "10:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                    

                }
                if (CompareTime_12_15 < CompareTime && CompareTime < CompareTime_14_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "12:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
                if (CompareTime_14_15 < CompareTime && CompareTime < CompareTime_16_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "14:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
                if (CompareTime_16_15 < CompareTime && CompareTime < CompareTime_18_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "16:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
            }
        }
        if (day == "Thursday")
        {
            foreach (Room a in timetable.Thursday.Rooms)
            {
                if (CompareTime_8_15 < CompareTime && CompareTime < CompareTime_10_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "08:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
                if (CompareTime_10_15 < CompareTime && CompareTime < CompareTime_12_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "10:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
                if (CompareTime_12_15 < CompareTime && CompareTime < CompareTime_14_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "12:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
                if (CompareTime_14_15 < CompareTime && CompareTime < CompareTime_16_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "14:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
                if (CompareTime_16_15 < CompareTime && CompareTime < CompareTime_18_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "16:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
            }
        }
        if (day == "Friday")
        {
            foreach (Room a in timetable.Friday.Rooms)
            {
                if (CompareTime_8_15 < CompareTime && CompareTime < CompareTime_10_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "08:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
                if (CompareTime_10_15 < CompareTime && CompareTime < CompareTime_12_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "10:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
                if (CompareTime_12_15 < CompareTime && CompareTime < CompareTime_14_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "12:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
                if (CompareTime_14_15 < CompareTime && CompareTime < CompareTime_16_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "14:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
                if (CompareTime_16_15 < CompareTime && CompareTime < CompareTime_18_15)
                {
                    foreach (Subject sub in a.Subject)
                    {
                        if (sub.Start == "16:15 UTC+2")
                        {
                            setSubjects(sub, a);
                        }
                    }
                }
            }
        }

        // kui õppehoones ei tiomu tunde, siis vaatajle antakse sellest teada
        if (!ThereIsSubjects)
        {
            if (!Est)
            {
                noSubjects.text = "Hetkel Delta õppehoones ei toimu tunde";
            }
            else
            {
                noSubjects.text = "There are no ongoing subjects in the Delta building";
            }
        }

        // tunniplaani keele muutmine (inglise keel ja eesti keel) 
        if (!Est)
        {
            Est = true;
        }
        else
        {
            Est = false;
        }

        
    }
    
    void setSubjects(Subject sub, Room a)
    {
        //tunniplaani nimetuse lühendamine kui AineNimetus.Length > 40
        ThereIsSubjects = true;
        if (!Est)
        {
            ruums.text = ruums.text + a.Id.ToString() + "\n";

            if (sub.Name.Length > 40)
            {
                string subname = sub.Name.Substring(0, 40);
                if (sub.Name.Substring(0, 40)[sub.Name.Substring(0, 40).Length - 1] == ' ')
                {
                    subjects.text = subjects.text + sub.Name.Substring(0, 40).Substring(0, sub.Name.Substring(0, 40).Length - 2) + "..." + "\n";
                }
                else if (sub.Name.Substring(0, 40)[sub.Name.Substring(0, 40).Length - 2] == ' ')
                {
                    subjects.text = subjects.text + sub.Name.Substring(0, 40).Substring(0, sub.Name.Substring(0, 40).Length - 3) + "..." + "\n";
                }
                else
                {
                    subjects.text = subjects.text + sub.Name.Substring(0, 40) + "..." + "\n";
                }


            }
            else
            {
                subjects.text = subjects.text + sub.Name + "\n";
            }

        }
        else
        {
            ruums.text = ruums.text + a.Id.ToString() + "\n";
            if (sub.Name_Eng.Length > 40)
            {

                if (sub.Name_Eng.Substring(0, 40)[sub.Name_Eng.Substring(0, 40).Length - 1] == ' ')
                {
                    subjects.text = subjects.text + sub.Name_Eng.Substring(0, 40).Substring(0, sub.Name_Eng.Substring(0, 40).Length - 2) + "..." + "\n";
                }
                else if (sub.Name_Eng.Substring(0, 40)[sub.Name_Eng.Substring(0, 40).Length - 2] == ' ')
                {
                    subjects.text = subjects.text + sub.Name_Eng.Substring(0, 40).Substring(0, sub.Name_Eng.Substring(0, 40).Length - 3) + "..." + "\n";
                }
                else
                {
                    subjects.text = subjects.text + sub.Name_Eng.Substring(0, 40) + "..." + "\n";
                }


            }
            else
            {
                subjects.text = subjects.text + sub.Name_Eng + "\n";
            }
        }
    }
}
