﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class roomTextSize : MonoBehaviour {

    // klass mis muudab ruumi numbrite suurust (nagu ainete nimetustel)

	public GameObject subject;
	Text mtext;
	Text mtext2;

	void Start () {
		mtext2 = GetComponent<Text> ();
	}
	
	void Update () {
		mtext = subject.GetComponents<Text> ()[0];
		mtext2.fontSize = mtext.cachedTextGenerator.fontSizeUsedForBestFit;
	}
}
