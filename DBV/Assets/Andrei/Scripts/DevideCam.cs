﻿using UnityEngine;

public class DevideCam : MonoBehaviour
{
    // siin klassis eraldatakse ekraan kaheks kaameraks: esimene kaamera näitab tunniplaani, teine näitab Delta õppehoone 3D visualisatsiooni
    public Camera m_SecondCamera;
    Camera m_FirstCamera;

    void Start()
    {
		m_SecondCamera.gameObject.SetActive (true);
        m_SecondCamera.enabled = false;
        m_SecondCamera.pixelRect = new Rect(0, 0, 350, Screen.height); // esimese kaamera mõõdud
        
        m_FirstCamera = Camera.main;
        m_FirstCamera.pixelRect = new Rect(350, 0, Screen.width - 350, Screen.height); // teise kaamera mõõdud

        ToggleCamera(m_SecondCamera, m_FirstCamera);
    }


    
    void ToggleCamera(Camera cameraToggle, Camera cameraOutput)
    {
        cameraToggle.enabled = !cameraToggle.enabled;
    }
}