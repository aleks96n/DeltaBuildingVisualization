﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectController {

    // antud klass vastutab animatsioonide käima panemisele

    public IEnumerator makePuddles(List<GameObject> puddles, Animator puddleAnim, int waitSec)
    {
        // lompide "kasvamine"
        yield return new WaitForSeconds(waitSec);
        foreach (GameObject puddle1 in puddles)
        {
            puddleAnim = puddle1.GetComponent<Animator>();
            puddleAnim.SetTrigger("inc");
        }
    }

    public IEnumerator makeSnowHills(List<GameObject> snowHillsObj, Animator snowHillsAnim, int waitSec)
    {
        // lumehangede "kasvamine"
        yield return new WaitForSeconds(waitSec);
        foreach (GameObject hill in snowHillsObj)
        {
            snowHillsAnim = hill.GetComponent<Animator>();
            snowHillsAnim.SetTrigger("inc");
        }
    }


    public IEnumerator deletePuddles(List<GameObject> puddles, Animator puddleAnim, int waitSec)
    {
        // lompide "aurumine"
        yield return new WaitForSeconds(waitSec);
        foreach (GameObject puddle1 in puddles)
        {
            puddleAnim = puddle1.GetComponent<Animator>();
            puddleAnim.SetTrigger("dec");
        }
    }

    public IEnumerator deleteSnowHills(List<GameObject> snowHillsObj, Animator snowHillsAnim, int waitSec)
    {
        // lumehangede "sulamine"
        yield return new WaitForSeconds(waitSec);
        foreach (GameObject hill in snowHillsObj)
        {
            snowHillsAnim = hill.GetComponent<Animator>();
            snowHillsAnim.SetTrigger("dec");
        }
    }
}
