﻿using System;

// JSON abiklass 

[System.Serializable]
public class Source
{
    public string id;
    public string name;
    public string self;
}

[System.Serializable]
public class Data
{
    public DateTime creationTime;
    public DateTime time;
    public string id;
    public string self;
    public Source source;
    public string text;
    public string type;
}

[System.Serializable]
public class PostManData
{
    public string id;
    public Data data;
    public string channel;
}