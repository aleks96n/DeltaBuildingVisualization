﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunCollor {

    // skript visualiseerib äikest muudates suundvalgusallika heledust

    public Light light;
    
    public IEnumerator lightning(Light light)
    {
        light.intensity = 2f;
        yield return new WaitForSeconds(0.02f);
        light.intensity = 1;
        yield return new WaitForSeconds(0.3f);
        light.intensity = 2.5f;
        yield return new WaitForSeconds(0.02f);
        light.intensity = 2;
        yield return new WaitForSeconds(0.05f);
        light.intensity = 1;
        
    }

    
}
