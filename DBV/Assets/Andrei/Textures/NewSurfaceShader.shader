﻿Shader "Custom/SnowEffect2" {
    Properties {
        _SnowColor ("Snow color", Color) = (157, 200, 68, 1)
        _MainTex ("MainTexture", 2D) = "white" {}
        _Bump ("BumpTex", 2D) = "bump" {}
        _SnowDirection ("Snow direction", Vector) = (0, 100, 0)
        _SnowLevel ("Amount of Snow", Range(2, -1)) = 0
    }
 
    SubShader {
        Tags { "RenderType" = "Opaque" }
        LOD 200
 
        CGPROGRAM
        #pragma surface surf Lambert
 
        half4 _SnowColor;
        sampler2D _MainTex;
        sampler2D _Bump;
        half3 _SnowDirection;
        fixed _SnowLevel;
 
        struct Input {
            float2 uv_MainTex;
            float2 uv_Bump;
            float3 worldNormal;
            INTERNAL_DATA
        };
 
        void surf(Input IN, inout SurfaceOutput o)
        {
            half4 tex = tex2D(_MainTex, IN.uv_MainTex);
            o.Normal = UnpackNormal (tex2D(_Bump, IN.uv_Bump));
			//float n = noise(x, y);
			//if (n > _SnowLevel) {
				//snow color
			//	o.Albedo = _SnowColor.rgb;
			//}

            if(dot(WorldNormalVector(IN, o.Normal), _SnowDirection) >= _SnowLevel)
            {
                o.Albedo = _SnowColor.rgb;
            }
            else
            {
                o.Albedo = tex.rgb;
            }
        }
        ENDCG
    }
}