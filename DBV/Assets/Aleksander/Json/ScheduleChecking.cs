﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.AI;

public class ScheduleChecking : MonoBehaviour
{

    private SubjectType RST;
    private Dictionary<string, List<string>> roomToDate;
    public GameObject educator;
    private GameObject[] whereToSpawn;

    // Use this for initialization
    // read the file and check every minute if an Actor has to spawn
    void Start()
    {
        RST = new SubjectType();
        roomToDate = RST.FileReader("Assets\\Aleksander\\Json\\timetable.json");
        whereToSpawn = GameObject.FindGameObjectsWithTag("Respawn");
        StartCoroutine("CheckEveryMinute");
    }
    // the method that checks the file to see if an Actor has to spawn
    IEnumerator CheckEveryMinute()
    {

        //this will work until the program closes
        while (true)
        {
            Debug.Log("Doing stuff");
            foreach (KeyValuePair<string, List<string>> dict in roomToDate)
            {
                foreach (string time in dict.Value)
                {
                    Debug.Log("Doing stuff here too  " + time);
                    if (DateTime.Now.TimeOfDay.ToString().Substring(0, 5).Equals(time))
                    {
                        SpawnEducator(dict.Key, returnRandomRemovePoint());
                        break;
                    }
                }
            }
            yield return new WaitForSeconds(10);
        }
    }

    //The initial spawn from a Spawn point to an Educator point
    public void SpawnEducator(string roomname, Transform spawnPoint)
    {
        Debug.Log(roomname);
        GameObject someActor = Instantiate(educator, spawnPoint.position, Quaternion.identity);
        SeatPath path = RoomManager.GetInstance().GiveEducatorPosition(roomname, returnRandomRemovePoint());
        someActor.GetComponent<Actor>().SetDestination(path);
    }
    // returns a random despawn point. 
    public Transform returnRandomRemovePoint()
    {
        return whereToSpawn[UnityEngine.Random.Range(0, whereToSpawn.Length - 1)].transform;
    }

}
