﻿using UnityEngine;  // UnityEngine gives us general access.
//using UnityEditor;  // UnityEditor gives us editor-specific access.
using System.IO;
using System;
using System.Collections.Generic;


public class SubjectType
{

    //https://stackoverflow.com/questions/13297563/read-and-parse-a-json-file-in-c-sharp
    //https://unity3d.com/learn/tutorials/topics/scripting/loading-game-data-json



    //private string scheduleJsonFile = "timetable.json";

    public Dictionary<string, List<string>> FileReader(string fileName)
    {
        Dictionary<string, List<string>> roomToDate = new Dictionary<string, List<string>>();
        if (File.Exists(fileName))
        {
            string dataAsJson = File.ReadAllText(fileName);
            roomToDate = RoomToTimeReturner(dataAsJson);
        }
        return roomToDate;
    }
    // iterates over all the days and subjects and returns only the most necessary ones
    protected Dictionary<string, List<string>> SubjectIterator(List<Room> ListOfRooms)
    {
        Dictionary<string, List<string>> helper = new Dictionary<string, List<string>>();
        for (int i = 0; i < ListOfRooms.Count; i++)
        {
            List<string> toAddTimeHere = new List<string>();
            var RoomName = ListOfRooms[i];
            var ListOfSubjects = ListOfRooms[i].Subject;
            for (int x = 0; x < ListOfSubjects.Count; x++)
            {
                toAddTimeHere.Add(ListOfSubjects[x].Start.Split(' ')[0]);
            }
            helper.Add(ListOfRooms[i].Id, toAddTimeHere);
        }
        return helper;

    }


    //I will require a list of rooms and their time, something like
    //[{Room_1004: [12:00, 15:00]}, {Room_1005: [13.00, 14.00]}]
    //List<Dictionary<string,List<string>>>
    public Dictionary<string, List<string>> RoomToTimeReturner(string file)
    {
        TimetableJson scheduleObject = JsonUtility.FromJson<TimetableJson>(file);
        Dictionary<string, List<string>> toReturn = new Dictionary<string, List<string>>();
        Debug.Log(DateTime.Now.DayOfWeek.ToString());
        switch (DateTime.Now.DayOfWeek.ToString())
        {
            case ("Monday"):
                List<Room> ListOfMondayRooms = scheduleObject.Monday.Rooms;
                toReturn = SubjectIterator(ListOfMondayRooms);
                return toReturn;
            case ("Tuesday"):
                List<Room> ListOfTuedayRooms = scheduleObject.Tuesday.Rooms;
                toReturn = SubjectIterator(ListOfTuedayRooms);
                return toReturn;
            case ("Wednesday"):
                List<Room> ListOfWednesdayRooms = scheduleObject.Wednesday.Rooms;
                toReturn = SubjectIterator(ListOfWednesdayRooms);
                return toReturn;
            case ("Thursday"):
                List<Room> ListOfThursdayRooms = scheduleObject.Thursday.Rooms;
                toReturn = SubjectIterator(ListOfThursdayRooms);
                return toReturn;
            case ("Friday"):
                List<Room> ListOfFridayRooms = scheduleObject.Friday.Rooms;
                toReturn = SubjectIterator(ListOfFridayRooms);
                return toReturn;
            default:
                //educators should not work during weekends.
                Debug.Log("Weekend?");
                return toReturn;

        }
    }
}
