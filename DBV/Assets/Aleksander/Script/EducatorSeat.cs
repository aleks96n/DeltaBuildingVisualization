﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EducatorSeat : MonoBehaviour {

    private Animation anim;
    public GameObject educator;
    private float subjectStartTime;
    // Use this for initialization


    //same as SitDown
    public void LeaveClass(SeatPath destinationToGo)
    {
        anim = gameObject.GetComponent<Animation>();
        anim.Play("LeaveEducator");
        GameObject someActor = Instantiate(educator, gameObject.transform.position, Quaternion.identity);
        someActor.GetComponent<Actor>().SetDestination(destinationToGo);
    }
    // If reached, do one of the 3 random animations
    // Note that unlike Student Actors, we use Animation instead of Animator. Animators allow multiple Layers of animations. However, because we are not using
    // multiple layers, we will instead use Legacy animations.
    // Also, there is a bug with the Animator freezing at the starting stage for some reason...
    public void StartEducating(GameObject educator)
    {
        anim = gameObject.GetComponent<Animation>();
        string[] pickAtRandom = new string[3];
        pickAtRandom[0] = "EducatorStandingAnimation";
        pickAtRandom[1] = "EducatorStandingAnimation2";
        pickAtRandom[2] = "EducatorStandingAnimation3";
        anim.Play(pickAtRandom[Random.Range(0, pickAtRandom.Length)]);
        Destroy(educator);
        subjectStartTime = Time.time;
        StartCoroutine("SubjectDuration");

    }

    IEnumerator SubjectDuration()
    {
        while(Time.time < subjectStartTime + 5400)
        {
            yield return new WaitForSeconds(1F);
        }
        var remove = RoomManager.GetInstance().GetRandomRemoveRoom();
        LeaveClass(new SeatPath(remove.transform, remove.transform, new NavMeshPath()));
        yield break;
    }

}
