﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DoorOpen : MonoBehaviour {

    private Animator anim;
    public bool opened = false;
    public int expectedActors = 0;

	// Use this for initialization
	void Start () {
        anim = gameObject.GetComponent<Animator>();
    }
	
    //Open the door
    public void PlayAnimationOpen()
    {
        anim.ResetTrigger("Close");
        anim.SetTrigger("Open");
    }
    //Close the door
    public void PlayAnimationClose()
    {
        anim.ResetTrigger("Open");
        anim.SetTrigger("Close");
        

    }
    //Add that some Actors are expected to come close to the door
    public void AddExpectedActors()
    {
        expectedActors++;
    }
    //Decrease Actors. If is is the first time an Actor is being decreased, he needs to open the door because it has not been open yet. 
    public void DeceaseExpectedActors()
    {
        expectedActors--;
        if (!opened)
        {
            PlayAnimationOpen();
            opened = true;
        }
        // Because checking the proximity of an Actor to the Door is not the best solution (Coroutine might miss that an Actor is close to the Door, calculations might screw 
        //something up), we need to define an error threshold. In this case, it was observed that the best threshold was 3.
        if (expectedActors < 3)
        {
            PlayAnimationClose();
            opened = false;
            expectedActors = 0;
        }

    }

}
