﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// this was used to test performance of the visualisation
public class TestPerformance : MonoBehaviour
{
	//What Actor will spawn
    public GameObject actor;

	//A count of spawned Actors
    public int actorsCount = 0;
	//The limit of actors that are going to spawn
    protected int actorsLimit = 2010;
	//For testing reasons, remove later

    public int foundPathCount = 0;
	//All the spawn locations
    public List<Transform> spawns = new List<Transform>();

	//remove later, irrelevant in the build
    public GameObject actorsParent;

	protected RoomManager roomManager;
    private GameObject[] actorPool;

    void Start()
	{
        roomManager = RoomManager.GetInstance();
        
        //use this method to spawn 2010 Actors all at once
        for (int i = 0; i < 2010; i++)
        {
            Transform currentSpawn = spawns[Random.Range(0, spawns.Count)];
            GameObject someActor = Instantiate(actor, currentSpawn.position, Quaternion.identity);
            someActor.transform.parent = actorsParent.transform;
            SeatPath path = roomManager.GiveSeatUntilFull(currentSpawn);
            someActor.GetComponent<Actor>().SetDestination(path);
        }
        
        //StartCoroutine("Spawn");
    }

    IEnumerator Spawn()
    {
        while (actorsCount < actorsLimit)
        {
            int step = actorsCount + 10;
            for (; actorsCount < Mathf.Min(step, actorsLimit); actorsCount++)
            {
                Transform currentSpawn = spawns[Random.Range(0, spawns.Count)];
                GameObject someActor = Instantiate(actor, currentSpawn.position, Quaternion.identity);
                someActor.transform.parent = actorsParent.transform;
				SeatPath path = roomManager.GiveSeatUntilFull(currentSpawn);
				someActor.GetComponent<Actor> ().SetDestination (path);
            }
            yield return new WaitForSeconds(1f);
        }
        yield break;

    }
}
