﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*!
 * ActorLevelChanger
 * Changes the layer of Actors. This allows views to only render Actors when they are on a view-given floor
 * For example, if the view is set to only render first floor objects, then an Actor with a second floor layer will not be rendered by that view
 */
public class ActorLevelChanger : MonoBehaviour {

	void OnEnable()
    {
        StartCoroutine("CheckForLevelChange");
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }
    //This method changes the layer of an Actor. Because an object has multiple children, we need to iterate over every child and put a correct layer for each child
    protected void ChangeLayer(string whichLayer)
    {
        int newLayer = LayerMask.NameToLayer(whichLayer);
        if (gameObject.layer != newLayer)
        {
            gameObject.layer = newLayer;
			Recursion(transform, newLayer);
        }
    }
	//Recurise method to iterate over every child.
	private void Recursion(Transform parent, int layer){
		if(parent.childCount == 0)
			return;
		foreach(Transform child in parent.transform){
			child.gameObject.layer = layer;
            //check if you need this...
		}
	}
    //Enumerator is a function that does something every x second. In this case, the x is 0.5.
    IEnumerator CheckForLevelChange()
    {
        while (isActiveAndEnabled)
        {
            //second floor starts when y is above 4. That is when we change the layer to Second Floor. 
            if (transform.position.y > 4)
            {
                ChangeLayer("SecondFloor");
            }
            else
            {
                ChangeLayer("FirstFloor");
            }
            yield return new WaitForSeconds(0.5F);
        }
        yield break;

    }
}
