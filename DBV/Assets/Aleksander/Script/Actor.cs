﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Actor : MonoBehaviour
{

    public NavMeshAgent navMeshAgent;
    // destination, as in the seat
    public Transform destination;
    public bool stopped = false;
    public GameObject Door;
    private Animation DoorAnim;
    // currectLocation is the room the actor is going to go to
	public Transform currentLocation;
    private bool DoorInstance = true;
    private bool sent = false;
    private float startTime;

    private void Awake()
    {
        //Used in testing. Time when an Actor has first Spawned
        startTime = Time.realtimeSinceStartup;
    }
    void Start()
    {
        //this is used to avoid syncronized walking from Actors. One can assume that Student Actors had army training, however this assumption was not proven in this thesis.
        GetComponent<Animator>().SetFloat("NonSyncro", Random.Range(0.50F, 0.95F));
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }
    // This function is used to determine whether an Actor has reached his destination or not. Box Colliders are another way this can achieved, however that resulted in huge performance loss.
    IEnumerator DeterminePath()
    {
        // while cycle that checks if an Actor has reached his destination
        while ((transform.position - destination.transform.position).sqrMagnitude > 14F)
        {
            // A statement that checks if an Actor is on a shortcut. If he is, we need to check if it is an openable door.
            if (navMeshAgent.isOnOffMeshLink)
            {
                OffMeshLinkData offD = navMeshAgent.currentOffMeshLinkData;
				var helper = offD.offMeshLink.gameObject;
                try
                {
                    //Not all shortcuts have animators. 
                    helper.GetComponent<Animator>().SetTrigger("OpenSes");
                    
                }
                catch
                {
                }
                
                navMeshAgent.CompleteOffMeshLink();
            }
            // Pre cashed paths only give Actors a path from their starting position to their room, not to their seat. So we let NavMeshAgent calculation to figure out the rest of the path
            if(navMeshAgent.destination != destination.transform.position)
            {
                navMeshAgent.SetDestination(destination.transform.position);
            }
            
              //the time it took an Actor to find a path from his starting position to his destination
              //used in testing
            if (navMeshAgent.hasPath && !sent)
            {
                
                sent = true;
                float endTime = Time.realtimeSinceStartup - startTime;
                RoomManager.GetInstance().WriteResult(endTime, destination.name);
            }
            
            
            yield return new WaitForSeconds(0.1F);
        }
        //if reached, remove gameObject and start sitting.
        // sometimes an actor is only given a room to go to, but not a seat
        // in these cases, the actor will stand idly.
        if (destination.GetComponent<Animator>() != null || destination.GetComponent<Animation>() != null)
        {
            Debug.Log(gameObject.tag + " got here");
            RemoveActorAndStartAnimation();
        }
        yield break;

    }

    //Destroy gameObject and start sitting
    //If an object is a "Remove" point, then the Actor just deletes himself
    private void RemoveActorAndStartAnimation()
    {
        if (gameObject.tag.Equals("Student"))
        {
            SeatPath seatPath = new SeatPath(currentLocation, destination, new NavMeshPath());
            destination.GetComponent<Seat>().SitDown(gameObject, seatPath);
        }
        else
        {
            Debug.Log(gameObject.tag + " got here");
            destination.gameObject.SetActive(true);
            destination.GetComponent<EducatorSeat>().StartEducating(gameObject);
        }
    }
    // Not all doors are shortcuts. This method checks the proximity of the door the Actor was given and if he is near it. If he is, call the Door method
    // and see if it needs opening or closing.
    IEnumerator NearDoorChecker()
    {
        while ((transform.position - Door.transform.position).sqrMagnitude > 8F)
        {
            yield return new WaitForSeconds(1F);
        }
        if (DoorInstance)
        {
            Door.GetComponent<DoorOpen>().DeceaseExpectedActors();
            DoorInstance = false;
        }
        yield break;
    }
    // Setting the destination of an Actor
	public void SetDestination(SeatPath seatPath)
    {
		StopCoroutine ("NearDoorChecker");
		destination = seatPath.getSeat();
		navMeshAgent.SetPath(seatPath.getPath());
		currentLocation = seatPath.getRoom();
		Door = SetDoor(currentLocation.gameObject);
        StartCoroutine("DeterminePath");
		if(Door != null)
			StartCoroutine("NearDoorChecker");
    }
    // Set the Door to the Actor
    public GameObject SetDoor(GameObject room)
    {
        foreach (Transform child in room.transform)
        {
            if (child.gameObject.tag.Equals("Door"))
            {
                child.GetComponent<DoorOpen>().AddExpectedActors();
                DoorInstance = true;
                return child.gameObject;
            }
        }
        return null;
    }

    public Transform GetDestination()
    {
        return destination.transform;
    }
}
