﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

// this class is not part of the accompanied files of the thesis
// it was used to calculate FPS and Actor Count when necesarry
public class DebugDisplay : MonoBehaviour
{

    protected int actorsCount = 0;
	public float deltaTime = 0.0f;

    public Text actorsLabel;
    public Text roomsLabel;
    //public Text cacheHitLabel;
    public Text cacheMissLabel;
	public Text FPS;
    public float bestFps = 0;
    public float worstFps = 1000;
    public List<string> values;
    private bool pressed;

    void Start()
    {
        pressed = false;
        values = new List<string>();
        //StartCoroutine("CountActors");
        StartCoroutine("AverageFPS");
    }
	
	void Update() //remove this later
    {
        if (!pressed)
        {
            deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
		    float msec = deltaTime * 1000.0f;
            float fps = 1.0f / deltaTime;
            if (fps > bestFps)
                bestFps = fps;
            if (fps < worstFps)
                worstFps = fps;
            string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
            FPS.text = text;
        
            values.Add(fps.ToString());
        }
        if (Input.GetKeyDown("m"))
        {
            pressed = true;
            Insert();
        }

    }

    IEnumerator AverageFPS()
    {
        while (true)
        {
            int counter = 0;
            float  sum = 0;
            bool bad = false;
            string fix;
            foreach(string s in values)
            {
                sum += float.Parse(s);
                counter++;
                
            }
            Insert(sum / counter);
            yield return new WaitForSeconds(5);
        }
    }

    public void Insert()
    {
        string path = "Assets/Aleksander/Script/Values.txt";

        //Write some text to the test.txt file
        StreamWriter writer = new StreamWriter(path, true);
        foreach (string s in values)
        {
            writer.WriteLine(s);
        }
        writer.Close();
    }

    public void Insert(float sum)
    {
        string path = "Assets/Aleksander/Script/Values.txt";

        //Write some text to the test.txt file
        StreamWriter writer = new StreamWriter(path, true);
        writer.WriteLine(sum.ToString());
        
        writer.Close();
    }

    IEnumerator CountActors()
    {
        while (true)
        {
            actorsCount = GameObject.FindGameObjectsWithTag("Player").Length;
            yield return new WaitForSeconds(2);
        }
    }
	

    public void OnRoomsLoaded(int count)
    {
        //roomsLabel.text = "Rooms: " + count;
    }

    public void AddActorsCount(int count = 1)
    {
        //actorsCount++;
        if (null != actorsLabel)
        {
            actorsLabel.text = "Actors: " + actorsCount;
        }
    }
}
