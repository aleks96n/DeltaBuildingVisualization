﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Seat : MonoBehaviour
{
    public GameObject actorToSpawnOnLeave;
    public GameObject whereToSpawn;
    private RoomManager roomManager;
    private Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
        roomManager = RoomManager.GetInstance();
    }
    // Leave the seat and spawn a new Actor near the seat. Record to the RoomManager that a new actor has Spawned
    public void LeaveSeat(SeatPath seat)
    {
        anim.SetTrigger("Leave");
        anim.ResetTrigger("Sit");
        GameObject someActor = Instantiate(actorToSpawnOnLeave, whereToSpawn.transform.position, Quaternion.identity);
        someActor.GetComponent<Actor>().SetDestination(seat);
        roomManager.SaveAmountInRoom(seat.getSeat().gameObject, someActor);
    }

    // sit down and  remove an actor from the room as he is now "Seated"
    public void SitDown(GameObject student, SeatPath path)
    {
        roomManager.RemoveFromMemory(student, path.getRoom().gameObject);
        if(!path.getSeat().tag.Equals("Remove"))
            anim.SetTrigger("Sit");
        Destroy(student);
    }

}
