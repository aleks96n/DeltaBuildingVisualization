﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.AI;

public class SpawnBasedOnData : MonoBehaviour
{

    private RoomManager roomManager;
    public GameObject actor;
    private Dictionary<string, int> memory = new Dictionary<string, int>();
    public List<Transform> spawns = new List<Transform>();
    private Dictionary<GameObject, int> toRemove = new Dictionary<GameObject, int>();
    private Dictionary<GameObject, int> toAdd = new Dictionary<GameObject, int>();


    private int GlobalTestVariableForUseCases = 0; //This tracks steps of the current use case
    // Use this for initialization
    void Start()
    {
        roomManager = RoomManager.GetInstance(); //used to calculate amount of people in the room
        StartCoroutine("Spawn");
    }

    IEnumerator Spawn()
    {
        while (true)
        {
            //UseCaseTester();
            Sort();
            yield return new WaitForSeconds(60); //one minute
        }

    }

    public void SetCumulocityDate(string data, int amount)
    {
        if (memory.ContainsKey(data))
        {
            memory[data] = amount;
        }
        else
        {
            memory.Add(data, amount);
        }
    }

    private void Sort()
    {
        Debug.Log("Sorting..............");
        foreach(KeyValuePair<string, int> pair in memory)
        {
            Refactor(pair.Value, pair.Key);
        }
        memory = new Dictionary<string, int>();
        Distribute();
    }
    // when doing use cases, the author was quite silly and did not find a way to assert these use cases in way that is not
    // counting the Actors manually 
    private void UseCaseTester()
    {
        /*
        switch (GlobalTestVariableForUseCases)   //Use case 1
        {
            case (0):
                Refactor(30, "1022");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            case (1):
                Refactor(20, "1022");
                Refactor(10, "1004");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            case (2):
                Refactor(15, "1022");
                Refactor(20, "1004");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            case (3):
                Refactor(30, "1004");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            default:
                Debug.Log("Use case done");
                break;
        }
        */

        switch (GlobalTestVariableForUseCases)   //Use case 2
        {
            case (0):
                Refactor(150, "1037");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            case (1):
                Refactor(75, "1037");
                Refactor(32, "1022");
                Refactor(22, "1004");
                Refactor(14, "2047");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            case (2):
                Refactor(22, "1022");
                Refactor(12, "1004");
                Refactor(5, "2047");
                Refactor(39, "1021");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            default:
                Debug.Log("Use case done");
                break;
        }

        /*
        switch (GlobalTestVariableForUseCases)   //Not a use case
        {
            case (0):
                Refactor(100, "1037");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            case (1):
                Refactor(0, "1037");
                Refactor(20, "1004");
                Refactor(20, "1005");
                Refactor(40, "1020");
                Refactor(20, "1019");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            case (2):
                Refactor(100, "1037");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            default:
                break;
        }
        */
        /*
        switch (GlobalTestVariableForUseCases)   //Use case 3
        {
            case (0):
                Refactor(1, "1039");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            case (1):
                Refactor(256, "1004");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            case (2):
                Refactor(1, "1040");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            case (3):
                Refactor(113, "1021");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            case (4):
                Refactor(33, "1004");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            case (5):
                Refactor(35, "1005");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            case (6):
                Refactor(35, "1006");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            case (7):
                Refactor(35, "1007");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            case (8):
                Refactor(35, "1008");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            case (9):
                Refactor(61, "1015");
                GlobalTestVariableForUseCases++;
                Distribute();
                break;
            default:
                Debug.Log("Use case done");
                break;
        }
        */
    }

    //the sensor mock data will be based on JSON objects, not GameObject from Unity, so we need to Refactor strings into GameObjects.
    public void Refactor(int amount, string nameOfRoom)
    {

        Calculations(amount, GameObject.Find(nameOfRoom));

    }

    // amount is the amount of people going to a specific room. The specific room is marked by the parameter destination
    private void Calculations(int amount, GameObject destination)
    {
        //see how many people there are currently in the room
        int amountinroom = roomManager.AmountOfStudentsInRoom(destination);

        //if sensor data said that 1 person is coming and the schedule data said that an educator is coming, then we need to decrease the value
        if (roomManager.EducatorComing(destination))
        {
            amountinroom--;
        }
        if (amountinroom != amount) //meaning that there is no change in the amount of people, so we ignore
        {
            if (amountinroom < amount) //if more people are coming to the room, document it in a toAdd list
            {
                //roomManager.RemoveStudentsFromRoom(destination, amount);
                if (!toAdd.ContainsKey(destination))
                {
                    toAdd.Add(destination, amount - amountinroom);
                }
                else
                {
                    toAdd[destination] = amount - amountinroom;
                }
            }
            else //if there is less, we need to remove them from the room
            {
                if (!toRemove.ContainsKey(destination))
                {
                    toRemove.Add(destination, amount - amountinroom);
                }
                else
                {
                    toRemove[destination] = amount - amountinroom;
                }
            }
        }
    }

    //The initial plan is to distribute room to room. In the case that we cannot distribute room to room, but only remove or add, then DetermineChange handles that
    private void Distribute()
    {
        //check what rooms are getting filled
        foreach (GameObject room in toAdd.Keys.ToList())
        {
            //for each filled room, check if someone has entered other rooms
            foreach (GameObject negativeRoom in toRemove.Keys.ToList())
            {
                //if they have, we need to distribute them properly
                if (toRemove[negativeRoom] < 0)
                {
                    if (toAdd[room] >= -(toRemove[negativeRoom])) //imagine we need to fill 100 people, but the room we are looking at only has 5 people missing. Meaning we take the 5, subtract 5 from the 100 and tell the program that the 100 people room is getting 5 people.
                    {
                        Debug.Log(negativeRoom + " = " + toRemove[negativeRoom]);
                        Debug.Log(room + " = " + toAdd[room]);
                        roomManager.RemoveStudentsFromRoom(negativeRoom, -(toRemove[negativeRoom]), room);
                        toAdd[room] = toAdd[room] + toRemove[negativeRoom];
                        toRemove[negativeRoom] = 0;
                    }
                    else
                    {
                        Debug.Log(negativeRoom + " -=- " + toRemove[negativeRoom]);
                        Debug.Log(room + " -=-" + toAdd[room]);
                        roomManager.RemoveStudentsFromRoom(negativeRoom, toAdd[room], room); //in the case where there are more missing people than we can fill, we remember the amount of missing people and let them leave the building
                        toRemove[negativeRoom] = toRemove[negativeRoom] + toAdd[room];
                        toAdd[room] = 0;
                        continue;
                    }
                }
            }
        }
        DetermineChange();
    }

    private void DetermineChange()
    {
        //this for loop takes the rest of the people from the rooms that need to be filled and spawns new agents to fill the gaps.
        foreach (GameObject room in toAdd.Keys.ToList())
        {
            if (toAdd[room] > 0)
            {
                var helper = toAdd[room];
                var anotherHelper = room;
                Spawn(anotherHelper, helper);
                toAdd.Remove(room);
            }
        }

        //this does the opposite. All the left over people without rooms are leaving the building
        foreach (GameObject negativeRoom in toRemove.Keys)
        {
            roomManager.RemoveStudentsFromRoom(negativeRoom, toRemove[negativeRoom], roomManager.GetRandomRemoveRoom());
        }
    }
    //method to spawn actors
    private void Spawn(GameObject room, int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            Transform currentSpawn = spawns[Random.Range(0, spawns.Count)];
            GameObject someActor = Instantiate(actor, currentSpawn.transform.position, Quaternion.identity);
            roomManager.SaveAmountInRoom(room, someActor);
            SeatPath path = roomManager.FromSpawnDestination(room.transform, currentSpawn);
            someActor.GetComponent<Actor>().SetDestination(path);
        }
    }


}




