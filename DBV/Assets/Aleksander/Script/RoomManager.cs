﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;
using UnityEngine.AI;
using System;

public class RoomManager : MonoBehaviour
{
    public List<GameObject> roomContainers;
    public List<Transform> spawnPoints;
    public List<Transform> removePoints;
    //{StartPoint: {EndPoint: Path}, ... }
    //This Dictionary is used to give a precached path to Actors, if a precached path exists.
    private Dictionary<Transform, Dictionary<Transform, NavMeshPath>> cachepaths = new Dictionary<Transform, Dictionary<Transform, NavMeshPath>>();

	// List of all rooms in the visualisation
    protected List<Transform> rooms = new List<Transform>();

    //{Room: [Actor1, Actor2, ... , ActorN]}
    // This Dictionary is used to get Actors that have not yet reached their seats
    private Dictionary<GameObject, List<GameObject>> roomToActors = new Dictionary<GameObject, List<GameObject>>();

    //{1034: [{Seat.01, 0}, {Seat.02,1}]} Where seat 0 is free and seat 1 is occupied
    // This Dictionary is used to give an empty seat in a given room.
    private Dictionary<Transform, List<Dictionary<Transform, int>>> roomToSeats = new Dictionary<Transform, List<Dictionary<Transform, int>>>();

    //educatorToSeats is very similar, but it will have only one seat, which we will use to animate when needed
    private Dictionary<Transform, Dictionary<Transform, int>> educatorToSeats = new Dictionary<Transform, Dictionary<Transform, int>>();


    public static RoomManager instance; //Singleton

    void Awake()
    {
        var counter = 0;
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        foreach (GameObject roomContainer in roomContainers)
        {
            for (int i = 0; i < roomContainer.transform.childCount; i++)
            {
                rooms.Add(roomContainer.transform.GetChild(i));
                counter++;
            }
        }
        CountDictionaryRooms();
        FromSpawnToSeat();
        Debug.Log(counter);

    }
    // Gets the seat of each room, the occupation of seats and which room houses what seat.
    public void CountDictionaryRooms()
    {
        foreach (Transform room in rooms)
        {
            if (!roomToSeats.ContainsKey(room))
            {
                List<Dictionary<Transform, int>> toPut = new List<Dictionary<Transform, int>>();
                foreach (Transform child in room)
                {
                    if (child.gameObject.tag.Equals("Sit"))
                    {
                        Dictionary<Transform, int> helper = new Dictionary<Transform, int>();
                        helper.Add(child, 0);
                        toPut.Add(helper);
                    }
                    else if (child.gameObject.tag.Equals("Educator"))
                    {
                        Dictionary<Transform, int> helper = new Dictionary<Transform, int>();
                        helper.Add(child, 0);
                        educatorToSeats.Add(room, helper);
                    }
                }
                roomToSeats.Add(room, toPut);
            }
        }
    }
    //Prepath calculations from SpawnPoint-> Room, SpawnPoint -> RemovePoints, Room -> RemovePoints, Room -> Room.
    public void FromSpawnToSeat()
    {
		foreach (Transform spawn in spawnPoints) {
			Dictionary<Transform, NavMeshPath> helper1 = new Dictionary<Transform, NavMeshPath> ();
			foreach(Transform room in rooms){
				NavMeshPath path = new NavMeshPath ();
				NavMesh.CalculatePath (spawn.position, room.position, NavMesh.AllAreas, path);
				helper1.Add (room, path);
			}
			foreach (Transform remove in removePoints) {
				NavMeshPath path = new NavMeshPath ();
				NavMesh.CalculatePath (spawn.position, remove.position, NavMesh.AllAreas, path);
				helper1.Add (remove, path);
			}
			cachepaths.Add (spawn.transform, helper1);
		}

		foreach (Transform startRoom in rooms) {
			Dictionary<Transform, NavMeshPath> helper1 = new Dictionary<Transform, NavMeshPath> ();
			foreach(Transform endRoom in rooms){
				NavMeshPath path = new NavMeshPath ();
				NavMesh.CalculatePath (startRoom.position, endRoom.position, NavMesh.AllAreas, path);
				helper1.Add (endRoom, path);
			}
			foreach (Transform remove in removePoints) {
				NavMeshPath path = new NavMeshPath ();
				NavMesh.CalculatePath (startRoom.position, remove.position, NavMesh.AllAreas, path);
				helper1.Add (remove, path);
			}
			cachepaths.Add (startRoom.transform, helper1);
		}
    }
	//Gets a precalculated path for SpawnPoint -> Room/RemovePoint
    public SeatPath FromSpawnDestination(Transform ActorDestination, Transform ActorStartingPoint)
    {
		if (ActorDestination.tag.Equals ("Remove")) {
			return new SeatPath (ActorDestination, ActorDestination, cachepaths [ActorStartingPoint] [ActorDestination]);
		}
		List<Dictionary<Transform, int>> buffer2 = roomToSeats[ActorDestination];

		List<Dictionary<Transform, int>> buffer = roomToSeats[ActorDestination];

		for(int i = 0; i < buffer.Count; i++){
			foreach (KeyValuePair<Transform, int> KeySeatValueOccupation in buffer[i]) {
				if (KeySeatValueOccupation.Value == 0) {
					roomToSeats [ActorDestination] [i] [KeySeatValueOccupation.Key] = 1;
					return new SeatPath(ActorDestination, KeySeatValueOccupation.Key, cachepaths[ActorStartingPoint][ActorDestination]);
				}
			}
		}
		return GetRandomRemoveRoom(ActorStartingPoint);

    }

	// This method first iteratates over every room in the visualisation in order to find a free seat.
	// After that, it returns a SeatPath class, consisting of the room the Actor is going to,
	// a seat that the Actor is going to sit on and a pre calculated Path from his starting point to the room.
	// In order to see why this method does not return a path from his starting point to his seat, please 
	// read the thesis Chapter X
	public SeatPath GiveSeatUntilFull(Transform startingDestination)
    {
		//{1034: [{Seat.01, 0}, {Seat.02,1}]}
		Dictionary<Transform, List<Dictionary<Transform, int>>> buffer = roomToSeats;

		foreach(KeyValuePair<Transform, List<Dictionary<Transform, int>>> room in buffer){
			for(int i = 0; i < room.Value.Count; i++){
				foreach (KeyValuePair<Transform, int> KeySeatValueOccupation in room.Value[i]) {
					if (KeySeatValueOccupation.Value == 0) {
						roomToSeats [room.Key] [i] [KeySeatValueOccupation.Key] = 1;
						return new SeatPath(room.Key, KeySeatValueOccupation.Key, cachepaths[startingDestination][room.Key]);
					}
				}
			}
		}
		return GetRandomRemoveRoom(startingDestination);
    }

	//Returns a random "Remove" point. Same idea as the top method
	public SeatPath GetRandomRemoveRoom(Transform startingPoint)
	{
		Transform removePoint = removePoints [UnityEngine.Random.Range (0, removePoints.Count)];
		SeatPath seatPath = new SeatPath (startingPoint, removePoint, cachepaths [startingPoint] [removePoint]); 
		return seatPath;
	}

	public GameObject GetRandomRemoveRoom()
	{
		return removePoints [UnityEngine.Random.Range (0, removePoints.Count)].gameObject;
	}


    // Save an actor in a room. For the algorithm, we need to know how many Actors are in a Room.
    public void SaveAmountInRoom(GameObject room, GameObject student)
    {
		
        if (!room.tag.Equals("Remove"))
        {
            if (roomToActors.ContainsKey(room))
            {
                if (roomToSeats[room.transform].Count > roomToActors[room].Count) //we assume that a room cannot have more actors than seats
                {
                    roomToActors[room].Add(student);
                }
            }
            else
            {
                roomToActors.Add(room, new List<GameObject>());
                roomToActors[room].Add(student);

            }
        }
    }

    // Return the amount of actors in a given room.
    public int AmountOfStudentsInRoom(GameObject room)
    {
        var people = 0;
        if (!roomToActors.ContainsKey(room))
        {
            return 0;
        }
        foreach (Dictionary<Transform, int> seats in roomToSeats[room.transform])
        {
            foreach (KeyValuePair<Transform, int> seat in seats)
            {
                if (seat.Value == 1)
                {
                    people++;
                }
            }

        }
        return people;
    }

    //Actors are deleted when they reach their seat in order to to start an animation and have the camera not render them
    public void RemoveFromMemory(GameObject actor, GameObject room)
    {
        try
        {
            if (!room.tag.Equals("Remove"))
            {
                roomToActors[room].Remove(actor);
            }
        }
        catch
        {
        }
    }

    public void RemoveStudentsFromRoom(GameObject ActorStartingPoint, int amount, GameObject ActorDestination)
    {
        if (amount < 0)
        {
            amount = -amount;
        }

        //First find all the moving guys that have not reached their seat and move them to their new path. This happens when an Actor is too slow and has not reached his destination
        int x = 0;
        for (; x < amount; x++)
        {
			if (roomToActors.ContainsKey(ActorStartingPoint) && roomToActors[ActorStartingPoint].Count != 0)
            {
				var actor = roomToActors[ActorStartingPoint][0].GetComponent<Actor>();
				SeatPath path = FromSpawnDestination(ActorDestination.transform, ActorStartingPoint.transform);

                actor.SetDestination(path);
				SaveAmountInRoom(ActorDestination, actor.gameObject);
				roomToActors[ActorStartingPoint].Remove(actor.gameObject);

            }
            else
            {
                break;
            }
        }
        // Second, make the sitting Actor sit up and go to their new destination
		foreach (Dictionary<Transform, int> seats in roomToSeats[ActorStartingPoint.transform])
        {
            if (x == amount)
            {
                break;
            }
            foreach (Transform seat in seats.Keys.ToList())
            {
                if (seats[seat] == 1)
                {
					seat.gameObject.GetComponent<Seat>().LeaveSeat(FromSpawnDestination(ActorDestination.transform, ActorStartingPoint.transform));
                    seats[seat] = 0;
                    x++;
                    if (x == amount)
                    {
                        break;
                    }

                }
            }
        }
    }
    public bool EducatorComing(GameObject room)
    {
        if (!educatorToSeats.ContainsKey(room.transform))
        {
            return false;
        }
        foreach(KeyValuePair<Transform, int> pair in educatorToSeats[room.transform])
        {
            if(educatorToSeats[room.transform][pair.Key] == 1)
            {
                return true;
            }
        }
        return false;
    }

    // Educator seats are different and we do not want Student Actors to sit on them. That is why there is a separate method for Educators
    public SeatPath GiveEducatorPosition(string roomName, Transform leaveWhere)
    {
        GameObject room = GameObject.Find(roomName);
        foreach (KeyValuePair<Transform, int> pair in educatorToSeats[room.transform])
        {
            SeatPath seatPath = new SeatPath(room.transform, pair.Key.transform, cachepaths[leaveWhere][room.transform]);
            if (educatorToSeats[room.transform][pair.Key] == 1)
            {
                //if an educator is there, remove him and replace him
                pair.Key.GetComponent<EducatorSeat>().LeaveClass(seatPath);
            }
            educatorToSeats[room.transform][pair.Key] = 1;
            //if everything is right, then it will return the seat
            
            return seatPath;
        }
        //if the seat is not present, just return the room
        
        return new SeatPath(room.transform, room.transform, new NavMeshPath());
    }

    // This was done for testing, not needed anymore unless you want to test the performance yourself
    public void WriteResult(float secDifference, string nameOfDestination)
    {
        string path = "Assets/Aleksander/Script/Table.txt";

        //Write some text to the test.txt file
        StreamWriter writer = new StreamWriter(path, true);
        writer.WriteLine(secDifference + "; " + nameOfDestination);
        writer.Close();

    }

	public static RoomManager GetInstance()
	{
		return instance;
	}
}
