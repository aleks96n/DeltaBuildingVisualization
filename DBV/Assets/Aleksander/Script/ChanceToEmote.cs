﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChanceToEmote : MonoBehaviour {
    private Animator anim;
    public GameObject[] bubble;
    public GameObject checkIfActorIsActive;
    private CameraSwitchScreen cameraswitchscreen;
    private bool initiateTyping = false;
    // Use this for initialization

    void Start()
    {
        anim = GetComponent<Animator>();
        cameraswitchscreen = GameObject.FindGameObjectWithTag("Event").GetComponent<CameraSwitchScreen>();
        StartCoroutine("RandomChanceToEmote");
    }
	
    // resets the triggers to make animation calling multiple times possible
    IEnumerator RandomChanceToEmote()
    {
        if(HasParameter("Emotion", anim))
        {
            anim.ResetTrigger("Emotion");
            anim.SetTrigger("Loop");
        }
        else if (HasParameter("Emotion1", anim))
        {
            anim.ResetTrigger("Emotion1");
            anim.ResetTrigger("Emotion2");
            anim.ResetTrigger("Emotion3");
            anim.SetTrigger("Loop");
        }
        while (checkIfActorIsActive.activeInHierarchy == false)
        {
            yield return new WaitForSeconds(15.0F);
        }
        float randomNumber = Random.Range(0, 100);
        if (!initiateTyping)
        {
            initiateTyping = true;
            if (HasParameter("Loop", anim))
            {
                anim.ResetTrigger("Loop");
            }
            if (HasParameter("Type", anim))
            {
                anim.SetTrigger("Type");
            }
        }
        if (bubble == null)
            yield break;
        //8% chance to emote something
        while (randomNumber > 8)
        {
            yield return new WaitForSeconds(1.0F);
        }
        Emote();

        yield return new WaitForSeconds(8.0F);

    }

    //https://answers.unity.com/questions/571414/is-there-a-way-to-check-if-an-animatorcontroller-h.html
    public static bool HasParameter(string paramName, Animator animator)
    {
        foreach (AnimatorControllerParameter param in animator.parameters)
        {
            if (param.name == paramName)
                return true;
        }
        return false;
    }
    
    // emote an emotion...based on location
    public void Emote()
    {
        //TODO remove anim
        if (HasParameter("Emotion1", anim))
        {
            string[] pickRandom = new string[3];
            pickRandom[0] = "Emotion1";
            pickRandom[1] = "Emotion2";
            pickRandom[2] = "Emotion3";
            anim.SetTrigger(pickRandom[Random.Range(0, pickRandom.Length)]);
        }
        else
            anim.SetTrigger("Emotion");
        foreach (GameObject bub in bubble)
        {
            if(bub != null)
                bub.transform.LookAt(cameraswitchscreen.getActiveCamera().transform);
        }
    }
}
