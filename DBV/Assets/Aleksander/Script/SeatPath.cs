﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// Helper class that stores data
public class SeatPath {

	// Use this for initialization
	public Transform room;
	public Transform seat;
	public NavMeshPath path;
	public SeatPath(Transform setRoom, Transform setSeat, NavMeshPath setPath){
		this.room = setRoom;
		this.seat = setSeat;
		this.path = setPath;
	}

	public Transform getRoom(){
		return room;
	}

	public Transform getSeat(){
		return seat;
	}

	public NavMeshPath getPath(){
		return path;
	}
}
